/**
 * sdlsandbox: ExecutableFinder.h
 */

#ifndef H_SDLSANDBOX_EXECUTABLEFINDER
#define H_SDLSANDBOX_EXECUTABLEFINDER

#include <boost/filesystem.hpp>

/**
 * \brief Finds the path to the current executable.
 *
 * \return  The path to the current executable.
 */
extern boost::filesystem::path find_executable();

#endif
