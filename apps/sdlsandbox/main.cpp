/**
 * sdlsandbox: main.cpp
 */

#include <cstdlib>
#include <iostream>

#include <boost/shared_ptr.hpp>

#include <SDL.h>

#if WITH_SDLttf
#include <SDL_ttf.h>
#endif

#include "ExecutableFinder.h"

void quit(const std::string& message, int code = EXIT_FAILURE)
{
  std::cerr << message << '\n';
  SDL_Quit();
  exit(code);
}

boost::filesystem::path resources_dir()
{
  boost::filesystem::path p = find_executable(); // sdlsandbox/build/bin/apps/sdlsandbox/sdlsandbox(.exe)
  p = p.parent_path();                           // sdlsandbox/build/bin/apps/sdlsandbox/
  p = p / "resources/";                          // sdlsandbox/build/bin/apps/sdlsandbox/resources/
  return p;
}

int main(int argc, char *argv[])
try
{
  // TODO: Add code here.
  return 0;
}
catch(std::exception& e)
{
  std::cerr << "Error: " << e.what() << '\n';
  return EXIT_FAILURE;
}
